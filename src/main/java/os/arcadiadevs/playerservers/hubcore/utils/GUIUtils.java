package os.arcadiadevs.playerservers.hubcore.utils;

import com.cryptomorin.xseries.XMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import os.arcadiadevs.playerservers.hubcore.database.DataBase;
import os.arcadiadevs.playerservers.hubcore.database.structures.DBInfoStructure;
import os.arcadiadevs.playerservers.hubcore.database.structures.PingInfoStructure;
import os.arcadiadevs.playerservers.hubcore.menusystem.PageMenu;
import os.arcadiadevs.playerservers.hubcore.menusystem.PlayerMenuUtility;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.bukkit.Bukkit.getServer;
import static os.arcadiadevs.playerservers.hubcore.PSHubCore.PSH;
import static os.arcadiadevs.playerservers.hubcore.utils.ColorUtils.translate;

public class GUIUtils extends PageMenu {
    public GUIUtils(PlayerMenuUtility playerMenuUtility) {
        super(playerMenuUtility);
    }

    @Override
    public String getMenuName() {
        return ChatColor.GREEN + "Server Selector";
    }

    @Override
    public int getSlots() {
        return 9 * 6;
    }

    @Override
    public void handleMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        DataBase db = new DataBase();

        if (Objects.requireNonNull(e.getCurrentItem()).getType().equals(XMaterial.BARRIER.parseMaterial())) {
            p.closeInventory();
            return;
        }

        List<DBInfoStructure> servers = db.getServersInfo();

        if (e.getCurrentItem().getType().equals(Material.DARK_OAK_BUTTON)) {
            if (ChatColor.stripColor(Objects.requireNonNull(e.getCurrentItem().getItemMeta()).getDisplayName()).equalsIgnoreCase("Left")) {
                if (page == 0){
                    p.sendMessage(ChatColor.GRAY + "You are already on the first page.");
                } else {
                    page = page - 1;
                    super.open();
                }
            } else if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Right")) {
                if (isNextPageAvailable(servers, index + 1)) {
                    page = page + 1;
                    super.open();
                } else {
                    p.sendMessage(ChatColor.GRAY + "You are on the last page.");
                }
            }
        }
    }

    private boolean isNextPageAvailable(List<DBInfoStructure> servers, int page) {
        return servers.size() > page * 28;
    }

    @Override
    public void setMenuItems() {

        DataBase db = new DataBase();
        PingUtil pu = new PingUtil();

        addMenuBorder();

        if (db.getServerCount() != null && db.getServerCount() != 0) {
            for (int i = 0; i < getMaxItemsPerPage(); i++) {
                index = getMaxItemsPerPage() * page + i;
                if (index >= db.getServerCount()) break;
                new Thread(() -> {
                    for (DBInfoStructure is : db.getServersInfo()) {
                        ItemStack istack;
                        if (pu.isOnline("127.0.0.1", is.getPort())) {

                            PingInfoStructure pus = pu.getData(Integer.parseInt(is.getPort()));

                            istack = new ItemStack(Objects.requireNonNull(XMaterial.EMERALD_BLOCK.parseMaterial()));
                            ItemMeta ir = istack.getItemMeta();
                            //noinspection ConstantConditions
                            ir.setDisplayName(translate("&a" + is.getPlayerName() + "'s server"));
                            ArrayList<String> lore = new ArrayList<>();
                            lore.add(translate("&cPort: &7" + is.getPort()));
                            lore.add(translate("&cUUID: &7" + is.getServerID().split("-")[0]));
                            lore.add(translate(String.format("&cOnline: &7%d/%d", pus.getOnline(), pus.getMax())));
                            lore.add(translate("&cMOTD: &7" + pus.getMOTD()));
                            ir.setLore(lore);
                            istack.setItemMeta(ir);

                            inventory.addItem(istack);
                        }
                    }

                    for (DBInfoStructure is : db.getServersInfo()) {
                        if (!pu.isOnline("127.0.0.1", is.getPort())) {
                            ItemStack istack = new ItemStack(Objects.requireNonNull(XMaterial.REDSTONE_BLOCK.parseMaterial()));
                            ItemMeta ir = istack.getItemMeta();
                            //noinspection ConstantConditions
                            ir.setDisplayName(translate("&a" + is.getPlayerName() + "'s server"));
                            ArrayList<String> lore = new ArrayList<>();
                            lore.add(translate("&cPort: &7" + is.getPort()));
                            lore.add(translate("&cUUID: &7" + is.getServerID().split("-")[0]));
                            ir.setLore(lore);
                            istack.setItemMeta(ir);

                            inventory.addItem(istack);
                        }
                    }

                    Bukkit.getScheduler().runTask(PSH, () -> playerMenuUtility.getOwner().openInventory(inventory));
                }).start();
            }
        }
    }
    }

